package de.bingen.epro.data.mongo.entities;

public abstract class AbstractEntity {
	
	public abstract Long getId();
	
	public abstract void setId(Long id);

}
