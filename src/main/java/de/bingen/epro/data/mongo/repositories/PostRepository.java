package de.bingen.epro.data.mongo.repositories;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;



import de.bingen.epro.data.mongo.entities.Post;


public interface PostRepository extends MongoRepository<Post, ObjectId>{

}
