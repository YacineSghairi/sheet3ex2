package de.bingen.epro;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.util.Assert;

import de.bingen.epro.data.mongo.service.MongoSamplePostService;

public class App {
	
    public static void main( String[] args ){
    	AnnotationConfigApplicationContext context = 
				new AnnotationConfigApplicationContext(BaseConfiguration.class);
    	Assert.notNull(context);
    	
    	MongoSamplePostService mongoSamplePostService = (MongoSamplePostService) context.getBean("mongoSamplePostService");
		mongoSamplePostService.savePost();
		
    }
}
