package de.bingen.epro.data.mongo.entities;

import java.util.Date;

import org.springframework.data.annotation.PersistenceConstructor;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;


@Document(collection="comment")
public class Comment extends AbstractDocument {
	
	@Field 
	@Indexed(unique=true)
	private String title;
	
	@Field 
	private Date date;
	
	@Field 
	private String author;
	
	@Field 
	private String text;
	
	@PersistenceConstructor
	public Comment(String title, Date date, String author, String text) {
		this.title = title;
		this.date = date;
		this.author = author;
		this.text = text;
	}
	
	public Comment(){
		
	}
	
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

}
