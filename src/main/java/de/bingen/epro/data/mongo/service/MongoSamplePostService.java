package de.bingen.epro.data.mongo.service;

import java.util.Arrays;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import de.bingen.epro.data.mongo.entities.Comment;
import de.bingen.epro.data.mongo.entities.Post;
import de.bingen.epro.data.mongo.repositories.CommentRepository;
import de.bingen.epro.data.mongo.repositories.PostRepository;

@Service
public class MongoSamplePostService {

	@Autowired
	private PostRepository postRepository;
	
	@Autowired
	private CommentRepository commentRepository;
	
	
	
	public void savePost() {
		Comment comment = new Comment("iojoijio", new Date(), "Hallo ich", "jiojiojioj");
		commentRepository.save(comment);
		
		Post post = new Post("ojiojio", "iojiojio", new Date(), Arrays.asList("ah", "ho"), "jiojiojioj", "oijiojioj", Arrays.asList(comment));
		postRepository.save(post);
	}
	
}
