package de.bingen.epro.data.mongo.repositories;

import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

import de.bingen.epro.data.mongo.entities.Comment;

public interface CommentRepository extends MongoRepository<Comment, ObjectId>{
		List<Comment> findByTitle(String title);
}
