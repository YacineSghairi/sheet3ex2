package de.bingen.epro.data.mongo.entities;

import java.util.Date;
import java.util.List;

import org.springframework.data.annotation.PersistenceConstructor;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Document(collection="post")
public class Post extends AbstractDocument {
	
	@Field 
	@Indexed(unique=true)
	private String title;
	
	@Field 
	@Indexed(unique=true)
	private String author;
	
	@Field
	private Date date;
	
	@Field
	private List<String> tags;
	
	@Field
	private String description;
	
	@Field
	private String text;
	

	@DBRef
	@Field("comment")
	private List<Comment> comments;
	
	public Post(){}
	
	@PersistenceConstructor
	public Post(String title, String author, Date date, List<String> tags, String description, String text,
			List<Comment> comments) {
		this.title = title;
		this.author = author;
		this.date = date;
		this.tags = tags;
		this.description = description;
		this.text = text;
		this.comments = comments;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public List<String> getTags() {
		return tags;
	}

	public void setTags(List<String> tags) {
		this.tags = tags;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public List<Comment> getComments() {
		return comments;
	}

	public void setComments(List<Comment> comments) {
		this.comments = comments;
	}
	
	

}
