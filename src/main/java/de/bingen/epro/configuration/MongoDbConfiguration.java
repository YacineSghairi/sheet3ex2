package de.bingen.epro.configuration;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.core.io.ClassPathResource;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.data.mongodb.config.AbstractMongoConfiguration;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.SimpleMongoDbFactory;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.mongodb.Mongo;
import com.mongodb.MongoClient;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;
import com.mongodb.WriteConcern;

@Configuration
@EnableTransactionManagement
@ComponentScan(basePackages = "de.bingen.epro.data.mongo")
@EnableMongoRepositories(basePackages = {"de.bingen.epro.data.mongo.repositories"})
public class MongoDbConfiguration {
	
	@Configuration
	@Profile("default")
	static class Default extends AbstractMongoConfiguration {
		
		@Value("${database.nosql.host}")
		private String databaseHost;
		
		@Value("${database.nosql.user}")
		private String databaseUsername;
		
		@Value("${database.nosql.password}")
		private String databasePassword;
		
		@Value("${database.nosql.port}")
		private int databasePort;
		
		@Value("${database.nosql.database}")
		private String databaseName;
		
		@Bean
		public static PropertyPlaceholderConfigurer nosqlPropertyPlaceholderConfigurer() {
			PropertyPlaceholderConfigurer propertyPlaceholderConfigurer = new PropertyPlaceholderConfigurer();
			propertyPlaceholderConfigurer.setLocation(new ClassPathResource("application-model-nosql.properties"));
			propertyPlaceholderConfigurer.setIgnoreUnresolvablePlaceholders(true);
			return propertyPlaceholderConfigurer;
		}
		
		@Override
		protected String getDatabaseName() {
			return databaseName;
		}
		
		@Override
	    protected String getMappingBasePackage() {
	        return "de.bingen.epro.data.mongo";
	    }
		
		@Override
		public Mongo mongo() throws Exception {
			MongoClient mongoClient = new MongoClient(new ServerAddress(databaseHost, databasePort), Arrays.asList(getMongoCredentials()));
			mongoClient.setWriteConcern(WriteConcern.UNACKNOWLEDGED);
			return mongoClient;
		}
		
		protected MongoCredential getMongoCredentials() {
			return MongoCredential.createCredential(databaseUsername, 
					databaseName, databasePassword.toCharArray());
		}
		
		@Bean
		public MongoDbFactory getMongoDbFactory() throws Exception {
			return new SimpleMongoDbFactory((MongoClient)mongo(), getDatabaseName());
		}

	}

}
